import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import helper from '../../helpers';
import decode from "jwt-decode";
import {cpfMask, telefoneMask, cepMask} from "../mask/mask";
import moment from 'moment';
import 'moment/locale/pt-br';

export default class PacienteApi {

    static state = {
        paciente: {
            nome:"",
            rg:"",
            cpf:"",
            orgaoExpeditor:"",
            dataNascimento:"",
            sexo:"",
            estadoCivil:"",
            profissao:"",
            naturalidadeCidadeId:"",
            nacionalidade:"",
            reponsavelPaciente: {},
            conjugue: "",
            celular: "",
            email: "",
            telefone: "",
        },
        responsavelPaciente: {
            nome:"",
            rg:"",
            cpf:"",
            orgaoExpeditor:"",
            dataNascimento:"",
            sexo:"",
            estadoCivil:"",
            profissao:"",
            naturalidadeCidadeId:"",
            nacionalidade:"",
            reponsavelPaciente: {},
            conjugue: "",
            celular: "",
            email: "",
            telefone: "",
        },
        endereco: {
            cep:"",
            logradouro:"",
            numero:"",
            setor:"",
            cidadeId:""
        },
        enderecoProfissional: {
            cep:"",
            logradouro:"",
            numero:"",
            setor:"",
            cidadeId:""
        },
        estados: [],
        cidades: [],
        estadosProfissional: [],
        cidadesProfissional: [],
        estadosNaturalidade: [],
        cidadesNaturalidade: [],
        estadosNaturalidadeResponsavel: [],
        cidadesNaturalidadeResponsavel: [],
        cidade:{},
        estado:{},
        cidadeProfissional:{},
        estadoProfissional:{},
        estadoNaturalidade:{},
        estadoNaturalidadeResponsavel:{},
        pacienteIdEditar: "", 
        modalPaciente: false
    }

    static buscarPessoa () {
        fetch(`${this.path}/pessoa/pessoaPorUsuarioId/` + this.state.usuarioId)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                pessoa: retorno.pessoa, 
                cpf: cpfMask(retorno.pessoa.pessoaFisica.cpf), 
                estado: retorno.pessoa.cidade == null ? "" : retorno.pessoa.cidade.uf.id
            }, this.buscarCidades);
            this.setState({
                pessoa:{
                    ...this.state.pessoa, 
                    telefone: telefoneMask(this.state.pessoa.telefone), 
                    cep: cepMask(this.state.pessoa.cep)
                }
            })
        });
    }

    static buscarCidades () {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estado)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidades: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    static buscarCidadesProfissional () {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoProfissional)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesProfissional: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    static buscarCidadesNaturalidade () {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoNaturalidade)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesNaturalidade: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    static buscarCidadesNaturalidadeResponsavel () {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoNaturalidadeResponsavel)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesNaturalidadeResponsavel: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    static buscarEstados () {
        debugger
        fetch(`${this.path}/uf/`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                estados:retorno.estados, 
                estadosNaturalidade: retorno.estados, 
                estadosProfissional: retorno.estados, 
                estadosNaturalidadeResponsavel: retorno.estados
            });
        });
    }

  static atualizaPacientes () {
      debugger
    fetch(`${this.path}/paciente/listarPacientes`)
      .then(response => response.json())
      .then(retorno => {
        this.setState({ pacientes: retorno.pacientes });
        this.montarRowsPacientes();
      });
  };

  static montarRowsPacientes() {
    this.dataPacientes = {
      columns: [
        { label: "Nome", field: "nome", sort: "asc", width: 200 },
        { label: "E-mail", field: "email", sort: "asc", width: 200 },
        { label: "CPF", field: "cpf", sort: "asc", width: 100 },
        { label: "Telefone", field: "telefone", width: 100 },
        { label: "Editar", field: "editar", width: 30 },
        { label: "Excluir", field: "excluir", width: 30 }
      ],
      rows: []
    };

    this.state.pacientes.map(paciente => {
      this.dataPacientes.rows.push({
        nome: paciente.nome.toUpperCase(),
        email: paciente.email,
        cpf: paciente.cpf != null ? cpfMask(paciente.cpf) : "-",
        telefone: telefoneMask(paciente.telefone),
        editar: (
          <span className="btn btn-primary paddingButtonEdit" onClick={() => this.editarPaciente(paciente.id)}>
            <FontAwesomeIcon icon={faEdit} value={paciente} className="displayBlock" />
          </span>
        ),
        excluir: (
          <span className="btn btn-danger paddingButtonDelete" onClick={() => this.excluirPaciente(paciente)}>
            <FontAwesomeIcon icon={faTrash} className="displayBlock" />
          </span>
        )
      });
    });
    this.forceUpdate();
  }

  static excluirPaciente(paciente) {
    paciente.excluido = true;
    let enderecoProfissionalLocal =
      paciente.enderecoProfissional == null
        ? { cep: "", logradouro: "", numero: "", setor: "", cidadeId: "" }
        : paciente.enderecoProfissional;

    this.setState(
      {
        paciente: paciente,
        endereco: paciente.endereco,
        enderecoProfissional: enderecoProfissionalLocal
      },
      this.salvarPaciente
    );
  }

  static salvarPaciente () {
    fetch(`${this.path}/paciente/salvarPaciente`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        paciente: this.state.paciente,
        endereco: this.state.endereco,
        enderecoProfissional: this.state.enderecoProfissional,
        cpf: this.state.paciente.cpf.replace(/\D/g, ""),
        cpfResponsavel:
          this.state.responsavelPaciente != undefined
            ? this.state.responsavelPaciente.cpf.replace(/\D/g, "")
            : "",
        responsavel: this.state.responsavelPaciente
      })
    })
      .then(response => {
        return response.json();
      })
      .then(json => {
        this.limparForm();
        let mensagemCallback =
          json.excluido == true
            ? "Paciente excluido com sucesso!"
            : `Paciente salvo com sucesso.`;
        this.setState(
          {
            modalPaciente: false,
            swetAlert: true,
            mensagem: mensagemCallback
          },
          this.atualizaPacientes
        );
      });
  };

  static editarPaciente(pacienteId) {
    let enderecoProfissionalLocal = {
      cep: "",
      logradouro: "",
      numero: "",
      setor: "",
      cidadeId: ""
    };
    fetch(`${this.path}/paciente/pacientePorId/${pacienteId}`)
      .then(response => response.json())
      .then(retorno => {
        let dataNascimento = moment(retorno.paciente.dataNascimento).format(
          "YYYY-MM-DD"
        );
        this.setState({
          modalPaciente: true,
          paciente: retorno.paciente,
          responsavelPaciente:
            retorno.paciente.responsavelPaciente != null
              ? retorno.paciente.responsavelPaciente
              : null,
          endereco: retorno.paciente.endereco,
          estado: retorno.paciente.endereco.cidade.ufId,
          enderecoProfissional:
            retorno.paciente.enderecoProfissional != null
              ? retorno.paciente.enderecoProfissional
              : enderecoProfissionalLocal,
          estadoProfissional:
            retorno.paciente.enderecoProfissional != null
              ? retorno.paciente.enderecoProfissional.cidade.ufId
              : null,
          estadoNaturalidade: retorno.paciente.naturalidadeCidade.ufId
        });
        this.setState({
          paciente: { ...this.state.paciente, dataNascimento: dataNascimento }
        });
        this.buscarCidadesProfissional();
        this.buscarCidades();
        this.buscarCidadesNaturalidade();
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      });
  }

  static toggle = nomeModal => () => {
    let modal = "modal" + nomeModal;
    this.setState({
      [modal]: !this.state[modal]
    });
  };

  static consultarCEP () {
    var urlRestCEP = `https://viacep.com.br/ws/${this.state.endereco.cep.replace(/\D/g, "")}/json/`;
    fetch(urlRestCEP)
      .then(response => response.json())
      .then(retorno => {
        this.setState(
          {
            endereco: {
              ...this.state.endereco,
              cep: retorno.cep,
              logradouro: retorno.logradouro,
              setor: retorno.bairro,
              complemento: retorno.complemento,
              cidade: retorno.localidade
            },
            estado: this.state.estados.filter(
              estado => estado.nome == retorno.uf
            )[0].id
          },
          this.buscarCidades
        );

        setTimeout(() => {
          this.setState(
            {
              endereco: {
                ...this.state.endereco,
                cidadeId: this.state.cidades.filter(
                  cidade => cidade.nome == retorno.localidade
                )[0].id
              }
            },
            console.log(this.state.cidade)
          );
        }, 100);
      });
  };

  static consultarCEPProfissional () {
    var urlRestCEP = `https://viacep.com.br/ws/${this.state.enderecoProfissional.cep.replace(/\D/g, "")}/json/`;
    fetch(urlRestCEP)
      .then(response => response.json())
      .then(retorno => {
        this.setState(
          {
            enderecoProfissional: {
              ...this.state.enderecoProfissional,
              cep: retorno.cep,
              logradouro: retorno.logradouro,
              setor: retorno.bairro,
              complemento: retorno.complemento,
              cidade: retorno.localidade
            },
            estadoProfissional: this.state.estadosProfissional.filter(
              estado => estado.nome == retorno.uf
            )[0].id
          },
          this.buscarCidadesProfissional
        );

        setTimeout(() => {
          this.setState(
            {
              enderecoProfissional: {
                ...this.state.enderecoProfissional,
                cidadeId: this.state.cidadesProfissional.filter(
                  cidade => cidade.nome == retorno.localidade
                )[0].id
              }
            },
            console.log(this.state.cidade)
          );
        }, 100);
      });
  };

  static changeData(value) {
    console.log(value);
    this.setState({
      paciente: { ...this.state.paciente, dataNascimento: value }
    });
  }
}
