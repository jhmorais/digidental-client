import React, { Component } from 'react';
import './paciente.css';
import { MDBInput, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBDataTable, MDBDatePicker, MDBIcon } from 'mdbreact';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import Breadcrumb from '../breadcrumb/breadcrumb';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import helper from '../../helpers';
import decode from "jwt-decode";
import {cpfMask, telefoneMask, cepMask} from "../mask/mask";
import moment from 'moment';
import 'moment/locale/pt-br';

export default class Paciente extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paciente: {
                nome:"",
                rg:"",
                cpf:"",
                orgaoExpeditor:"",
                dataNascimento:"",
                sexo:"",
                estadoCivil:"",
                profissao:"",
                naturalidadeCidadeId:"",
                nacionalidade:"",
                reponsavelPaciente: {},
                conjugue: "",
                celular: "",
                email: "",
                telefone: "",
            },
            responsavelPaciente: {
                nome:"",
                rg:"",
                cpf:"",
                orgaoExpeditor:"",
                dataNascimento:"",
                sexo:"",
                estadoCivil:"",
                profissao:"",
                naturalidadeCidadeId:"",
                nacionalidade:"",
                reponsavelPaciente: {},
                conjugue: "",
                celular: "",
                email: "",
                telefone: "",
            },
            endereco: {
                cep:"",
                logradouro:"",
                numero:"",
                setor:"",
                cidadeId:""
            },
            enderecoProfissional: {
                cep:"",
                logradouro:"",
                numero:"",
                setor:"",
                cidadeId:""
            },
            estados: [],
            cidades: [],
            estadosProfissional: [],
            cidadesProfissional: [],
            estadosNaturalidade: [],
            cidadesNaturalidade: [],
            estadosNaturalidadeResponsavel: [],
            cidadesNaturalidadeResponsavel: [],
            cidade:{},
            estado:{},
            cidadeProfissional:{},
            estadoProfissional:{},
            estadoNaturalidade:{},
            estadoNaturalidadeResponsavel:{},
            pacienteIdEditar: "", 
            modalPaciente: false
        }
        this.path = helper.path();
    }

    componentDidMount(){
        this.buscarEstados()
        this.atualizaPacientes();
    }

    getUsuarioLogado = () => {
        const decoded = localStorage.getItem('userAuth') != null ? decode(localStorage.getItem('userAuth')) : null
            
        if(decoded != null){
            this.setState({usuarioId: decoded.sub.userId, tipoUsuario: decoded.sub.tipoUsuario}, this.buscarPessoa)
        }
    }

    buscarPessoa = () => {
        fetch(`${this.path}/pessoa/pessoaPorUsuarioId/` + this.state.usuarioId)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                pessoa: retorno.pessoa, 
                cpf: cpfMask(retorno.pessoa.pessoaFisica.cpf), 
                estado: retorno.pessoa.cidade == null ? "" : retorno.pessoa.cidade.uf.id
            }, this.buscarCidades);
            this.setState({
                pessoa:{
                    ...this.state.pessoa, 
                    telefone: telefoneMask(this.state.pessoa.telefone), 
                    cep: cepMask(this.state.pessoa.cep)
                }
            })
        });
    }

    buscarCidades = () => {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estado)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidades: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    buscarCidadesProfissional = () => {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoProfissional)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesProfissional: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    buscarCidadesNaturalidade = () => {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoNaturalidade)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesNaturalidade: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    buscarCidadesNaturalidadeResponsavel = () => {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estadoNaturalidadeResponsavel)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidadesNaturalidadeResponsavel: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    buscarEstados = () => {
        fetch(`${this.path}/uf/`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                estados:retorno.estados, 
                estadosNaturalidade: retorno.estados, 
                estadosProfissional: retorno.estados, 
                estadosNaturalidadeResponsavel: retorno.estados
            });
        });
    }

    limparForm(json){
        this.setState({
            paciente: {
                nome:"",
                rg:"",
                cpf:"",
                orgaoExpeditor:"",
                dataNascimento:"",
                sexo:"",
                estadoCivil:"",
                profissao:"",
                naturalidadeCidadeId:"",
                nacionalidade:"",
                reponsavelPaciente: {},
                conjugue: "",
                celular: "",
                email: "",
                telefone: "",
            },
            responsavelPaciente: {
                nome:"",
                rg:"",
                cpf:"",
                orgaoExpeditor:"",
                dataNascimento:"",
                sexo:"",
                estadoCivil:"",
                profissao:"",
                naturalidadeCidadeId:"",
                nacionalidade:"",
                reponsavelPaciente: {},
                conjugue: "",
                celular: "",
                email: "",
                telefone: "",
            },
            endereco: {
                cep:"",
                logradouro:"",
                numero:"",
                setor:"",
                cidadeId:""
            },
            enderecoProfissional: {
                cep:"",
                logradouro:"",
                numero:"",
                setor:"",
                cidadeId:""
            },
            cidades: [],
            cidadesProfissional: [],
            cidadesNaturalidade: [],
            cidadesNaturalidadeResponsavel: [],
            cidade:{},
            estado:{},
            cidadeProfissional:{},
            estadoProfissional:{},
            estadoNaturalidade:{},
            estadoNaturalidadeResponsavel:{},
            pacienteIdEditar: ""
        }, this.atualizaPacientes)
    }

    atualizaPacientes = () => {
        fetch(`${this.path}/paciente/listarPacientes`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({pacientes:retorno.pacientes});
            this.montarRowsPacientes();
        });
    }

    montarRowsPacientes(){
        this.dataPacientes = {
            columns: [
                {label: 'Nome', field: 'nome', sort: 'asc', width: 200},
                {label: 'E-mail',field: 'email', sort: 'asc', width: 200},
                {label: 'CPF', field: 'cpf', sort: 'asc', width: 100},
                {label: 'Telefone', field: 'telefone', width: 100},
                {label: 'Editar', field: 'editar', width: 30},
                {label: 'Excluir', field: 'excluir', width: 30},
            ],
            rows: []
        }

        this.state.pacientes.map(paciente => {
            this.dataPacientes.rows.push(
                {
                    nome: paciente.nome.toUpperCase(),
                    email: paciente.email,
                    cpf: paciente.cpf != null ? cpfMask(paciente.cpf) : "-",
                    telefone: telefoneMask(paciente.telefone),
                    editar:  
                    <span className="btn btn-primary paddingButtonEdit" onClick={() => this.editarPaciente(paciente.id)}>
                        <FontAwesomeIcon icon={faEdit} value={paciente} className="displayBlock" />
                    </span>,
                    excluir:  
                    <span className="btn btn-danger paddingButtonDelete" onClick={() => this.excluirPaciente(paciente)}>
                        <FontAwesomeIcon icon={faTrash} className="displayBlock" />
                    </span>
                }
            )
        })
        this.forceUpdate();
    }

    excluirPaciente(paciente){
        paciente.excluido = true
        let enderecoProfissionalLocal = paciente.enderecoProfissional == null ? 
            {cep:"", logradouro:"", numero:"", setor:"", cidadeId:""} : paciente.enderecoProfissional

        this.setState({paciente: paciente, endereco: paciente.endereco, enderecoProfissional: enderecoProfissionalLocal}, this.salvarPaciente)
    }

    salvarPaciente = () => {
        fetch(`${this.path}/paciente/salvarPaciente`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "paciente": this.state.paciente,
                "endereco": this.state.endereco,
                "enderecoProfissional": this.state.enderecoProfissional,
                "cpf": this.state.paciente.cpf.replace(/\D/g, ''),
                "cpfResponsavel": this.state.responsavelPaciente != undefined ? this.state.responsavelPaciente.cpf.replace(/\D/g, '') : '',
                "responsavel": this.state.responsavelPaciente
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.limparForm()
            let mensagemCallback = json.excluido == true ? 'Paciente excluido com sucesso!' : `Paciente salvo com sucesso.`
            this.setState({
                modalPaciente: false,
                swetAlert: true, 
                mensagem: mensagemCallback
            }, this.atualizaPacientes)
        });
    }

    editarPaciente(pacienteId) {
        let enderecoProfissionalLocal = {
            cep:"",
            logradouro:"",
            numero:"",
            setor:"",
            cidadeId:""
        }
        fetch(`${this.path}/paciente/pacientePorId/${pacienteId}`)
        .then(
            response => response.json()
        ).then(retorno => {
            debugger
            let dataNascimento = moment(retorno.paciente.dataNascimento).format('YYYY-MM-DD')
            this.setState({
                modalPaciente: true,
                paciente: retorno.paciente,
                responsavelPaciente: retorno.paciente.responsavelPaciente != null ? retorno.paciente.responsavelPaciente : null,
                endereco: retorno.paciente.endereco,
                estado: retorno.paciente.endereco.cidade.ufId,
                enderecoProfissional: retorno.paciente.enderecoProfissional != null ? retorno.paciente.enderecoProfissional : enderecoProfissionalLocal,
                estadoProfissional: retorno.paciente.enderecoProfissional != null ? retorno.paciente.enderecoProfissional.cidade.ufId : null,
                estadoNaturalidade: retorno.paciente.naturalidadeCidade != null ? retorno.paciente.naturalidadeCidade.ufId : null,
            })
            this.setState({paciente: {...this.state.paciente, dataNascimento: dataNascimento}})
            this.buscarCidadesProfissional()
            this.buscarCidades()
            this.buscarCidadesNaturalidade()
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        });
    }

    toggle = nomeModal => () => {
        let modal = 'modal' + nomeModal
        this.setState({
          [modal]: !this.state[modal]
        });
    }

    consultarCEP = () => {
        var urlRestCEP = `https://viacep.com.br/ws/${this.state.endereco.cep.replace(/\D/g, "")}/json/`
        fetch(urlRestCEP)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                endereco: {
                    ...this.state.endereco,
                    cep: retorno.cep == null ? '': retorno.cep,
                    logradouro: retorno.logradouro == null ? '': retorno.logradouro,
                    setor: retorno.bairro == null ? '': retorno.bairro,
                    complemento: retorno.complemento == null ? '': retorno.complemento,
                },
                estado: this.state.estados.filter(estado => estado.nome == retorno.uf)[0].id,
            }, this.buscarCidades)
            
            setTimeout(() => {
                if(this.state.cidades.length > 0){
                    this.setState({
                        endereco: {
                            ...this.state.endereco, 
                            cidadeId: this.state.cidades.filter(cidade => cidade.nome == retorno.localidade)[0].id
                        }
                    })
                }
            },300)
            
            
        });
    }

    consultarCEPProfissional = () => {
        var urlRestCEP = `https://viacep.com.br/ws/${this.state.enderecoProfissional.cep.replace(/\D/g, "")}/json/`
        fetch(urlRestCEP)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                enderecoProfissional: {
                    ...this.state.enderecoProfissional,
                    cep: retorno.cep == null ? '': retorno.cep,
                    logradouro: retorno.logradouro == null ? '': retorno.logradouro,
                    setor: retorno.bairro == null ? '': retorno.bairro,
                    complemento: retorno.complemento == null ? '': retorno.complemento,
                },
                estadoProfissional: this.state.estadosProfissional.filter(estado => estado.nome == retorno.uf)[0].id,
            }, this.buscarCidadesProfissional)

            setTimeout(() => {
                if(this.state.cidades.length > 0){
                    this.setState({
                        enderecoProfissional: {
                            ...this.state.enderecoProfissional, 
                            cidadeId: this.state.cidadesProfissional.filter(cidade => cidade.nome == retorno.localidade)[0].id
                        }
                    })
                }
            },300)
        });
    }

    changeData(value) {
        this.setState( {paciente: {...this.state.paciente, dataNascimento: value}} )
    }

    render(){
        return(
            <div id="content-father" className="container-fluid mt-5">

            <Breadcrumb paginaAtual="Paciente"/>

            <div className="row wow fadeIn">
                <div className="col-12">
                    <div className="card">

                        <h5 className="card-header blue-gradient white-text text-center py-4">
                            <strong>Paciente</strong>
                        </h5>

                        <div className="card-body px-lg-5 pt-4">
                            
                            <MDBBtn onClick={() => this.setState({modalPaciente: !this.state.modalPaciente})} className="mb-4">
                                <MDBIcon icon="user" className="mr-3"/>
                                Novo Paciente
                            </MDBBtn>
                            <h2 className="h3-responsive">Lista de Pacientes</h2>
                            <hr />

                            <MDBDataTable striped bordered hover data={this.dataPacientes} 
                                entriesLabel="Mostrar "
                                infoLabel={["Mostrando","até","de","entradas"]}
                                paginationLabel={["Anterior","Próximo"]}
                                searchLabel="Filtrar"
                                responsive="true"/>

                        </div>
                    </div>
                </div>
            </div>

            <MDBModal isOpen={this.state.modalPaciente} toggle={this.toggle('Paciente')} size="xl">
                <MDBModalHeader toggle={this.toggle('Paciente')}>Cadastro Paciente</MDBModalHeader>
                <MDBModalBody className="p-4">
                    <form className="text-left colorGrey">

                        <h2 className="h3-responsive titulo">Dados Pessoais</h2>
                        <hr className="linha-hr" />

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-10"> 
                                <MDBInput className="texto-uppercase" label="Nome" group type="text" validate error="wrong" success="right" value={this.state.paciente.nome} 
                                    onChange={e => { this.setState( {paciente: {...this.state.paciente, nome: e.target.value}} ) }}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-3">
                                <MDBInput label="CPF" id="cpf" group type="text" validate error="wrong" success="right" value={this.state.paciente.cpf} 
                                    onChange={e => this.setState( {paciente:{...this.state.paciente, cpf: cpfMask(e.target.value)} })}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-3">
                                <MDBInput label="RG" group type="text" validate error="wrong" success="right" value={this.state.paciente.rg} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, rg: e.target.value}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-3">
                                <MDBInput label="Orgão Expeditor" group type="text" validate error="wrong" success="right" value={this.state.paciente.orgaoExpeditor} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, orgaoExpeditor: e.target.value}} )}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-3">
                                <MDBInput label="Celular" id="celular" group type="text" validate error="wrong" length="10" success="right" value={this.state.paciente.celular} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, celular: telefoneMask(e.target.value)}} )}/>
                            </div>
                            
                            <div className="md-form col-sm-12 col-xl-3">
                                <MDBInput label="Telefone" id="telefone" group type="text" validate error="wrong" success="right" value={this.state.paciente.telefone} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, telefone: telefoneMask(e.target.value)}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="E-mail" group type="text" validate error="wrong" success="right" value={this.state.paciente.email} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, email: e.target.value}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Profissão" group type="text" validate error="wrong" success="right" value={this.state.paciente.profissao} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, profissao: e.target.value}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Data de Nascimento" group type="date" validate error="wrong" success="right" value={this.state.paciente.dataNascimento} 
                                    onChange={e => this.changeData(e.target.value)}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                <span>Sexo</span>
                                <br />
                                <select value={this.state.paciente.sexo} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, sexo: e.target.value}} )} 
                                    className="browser-default custom-select">
                                    <option value="" disabled>:: Selecione ::</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Feminino</option>
                                </select>
                            </div>

                            <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                <span>Estado Civil</span>
                                <br />
                                <select value={this.state.paciente.estadoCivil} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, estadoCivil: e.target.value}} )} 
                                    className="browser-default custom-select">
                                    <option value="" disabled>:: Selecione ::</option>
                                    <option value="S">Solteiro(a)</option>
                                    <option value="C">Casado(a)</option>
                                    <option value="D">Divorciado(a)</option>
                                    <option value="V">Viúvo(a)</option>
                                </select>
                            </div>

                            {this.state.paciente.estadoCivil == "C" ?
                                <div className="md-form col-sm-12 col-xl-4">
                                    <MDBInput label="Conjugue" group type="text" validate error="wrong" success="right" value={this.state.paciente.conjugue} 
                                        onChange={e => this.setState( {paciente: {...this.state.paciente, conjugue: e.target.value}} )}
                                        className="mt-4"/>
                                </div>
                                : <div></div>
                            }
                        </div>

                        <div className="row mt-4">

                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Contato de Emergencia" group type="text" validate error="wrong" success="right" value={this.state.paciente.contatoEmergencia} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, contatoEmergencia: e.target.value}} )}
                                    className="mt-4"/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                <span>Parentesco</span>
                                <br />
                                <select value={this.state.paciente.parentesco} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, parentesco: e.target.value}} )} 
                                    className="browser-default custom-select">
                                    <option value="" disabled>:: Selecione ::</option>
                                    <option value="A">Avô/Avó</option>
                                    <option value="F">Filho/Filha</option>
                                    <option value="I">Irmão/Irmã</option>
                                    <option value="M">Mãe</option>
                                    <option value="ME">Marido/Esposa</option>
                                    <option value="P">Pai</option>
                                    <option value="PR">Primo/Prima</option>
                                    <option value="T">Tio/Tia</option>
                                </select>
                            </div>

                        </div>

                        <div className="row mt-4">

                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Nacionalidade" group type="text" validate error="wrong" success="right" value={this.state.paciente.nacionalidade} 
                                    onChange={e => this.setState( {paciente: {...this.state.paciente, nacionalidade: e.target.value}} )}/>
                            </div>

                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Estado Naturalidade</span>
                                <br />
                                <select value={this.state.estadoNaturalidade} onChange={e => this.setState({estadoNaturalidade: e.target.value}, this.buscarCidadesNaturalidade)} className="browser-default custom-select">
                                    <option value="" disabled>Estado</option>
                                    {this.state.estadosNaturalidade.map(function(estado){
                                        return (
                                            <option key={estado.id} value={estado.id}>{estado.nome}</option>
                                        );
                                    }, this)}
                                </select>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Cidade Naturalidade</span>
                                <br />
                                <select value={this.state.paciente.naturalidadeCidadeId} onChange={e => this.setState( {paciente: {...this.state.paciente, naturalidadeCidadeId: e.target.value}} )} className="browser-default custom-select">
                                    <option value="" disabled>Cidade</option>
                                    { this.state.estadoNaturalidade != null ?
                                        this.state.cidadesNaturalidade.map(function(cidade){
                                            return (
                                                <option key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                                            );
                                        }, this)
                                        : <option value="" disabled>Cidade</option>
                                    }
                                </select>
                            </div>
                        </div>
                        {this.state.paciente.data != undefined && (moment(new Date()).diff(moment(this.state.paciente.data), 'years')) < 18 ?
                            <div>
                                <h2 className="h3-responsive titulo mt-4">Dados Responsável</h2>
                                <hr className="linha-hr" />

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-12"> 
                                        <MDBInput label="Nome" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.nome} 
                                            onChange={e => { this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, nome: e.target.value}} ) }}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="CPF" id="cpf" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.cpf} 
                                            onChange={e => this.setState( {responsavelPaciente:{...this.state.responsavelPaciente, cpf: cpfMask(e.target.value)} })}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="RG" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.rg} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, rg: e.target.value}} )}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="Orgão Expeditor" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.orgaoExpeditor} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, orgaoExpeditor: e.target.value}} )}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="Celular" id="celular" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.celular} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, celular: telefoneMask(e.target.value)}} )}/>
                                    </div>
                                    
                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="Telefone" id="telefone" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.telefone} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, telefone: telefoneMask(e.target.value)}} )}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="E-mail" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.email} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, email: e.target.value}} )}/>
                                    </div>
                                
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Profissão" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.profissao} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, profissao: e.target.value}} )}/>
                                    </div>
                                
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Data de Nascimento" group type="date" validate error="wrong" success="right" value={this.state.responsavelPaciente.data} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, data: e.target.value}} )}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                        <span>Sexo</span>
                                        <br />
                                        <select value={this.state.responsavelPaciente.sexo} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, sexo: e.target.value}} )} 
                                            className="browser-default custom-select">
                                            <option value="" disabled>:: Selecione ::</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Feminino</option>
                                        </select>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                        <span>Estado Civil</span>
                                        <br />
                                        <select value={this.state.responsavelPaciente.estadoCivil} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, estadoCivil: e.target.value}} )} 
                                            className="browser-default custom-select">
                                            <option value="" disabled>:: Selecione ::</option>
                                            <option value="S">Solteiro(a)</option>
                                            <option value="C">Casado(a)</option>
                                            <option value="D">Divorciado(a)</option>
                                            <option value="V">Viúvo(a)</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="row mt-4">
                                    {this.state.paciente.estadoCivil == "C" ?
                                        <div className="md-form col-sm-12 col-xl-6">
                                            <MDBInput label="Conjugue" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.conjugue} 
                                                onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, conjugue: e.target.value}} )}/>
                                        </div>
                                        : <div></div>
                                    }

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Nacionalidade" group type="text" validate error="wrong" success="right" value={this.state.responsavelPaciente.nacionalidade} 
                                            onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, nacionalidade: e.target.value}} )}/>
                                    </div>

                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <span>Estado Naturalidade</span>
                                        <br />
                                        <select value={this.state.estadoNaturalidadeResponsavel} onChange={e => this.setState({estadoNaturalidadeResponsavel: e.target.value}, this.buscarCidadesNaturalidadeResponsavel)} className="browser-default custom-select">
                                            <option value="" disabled>Estado</option>
                                            {this.state.estadosNaturalidade.map(function(estado){
                                                return (
                                                    <option key={estado.id} value={estado.id}>{estado.nome}</option>
                                                );
                                            }, this)}
                                        </select>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <span>Cidade Naturalidade</span>
                                        <br />
                                        <select value={this.state.responsavelPaciente.naturalidadeCidadeId} onChange={e => this.setState( {responsavelPaciente: {...this.state.responsavelPaciente, naturalidadeCidadeId: e.target.value}} )} className="browser-default custom-select">
                                            <option value="" disabled>Cidade</option>
                                            { this.state.estadoNaturalidadeResponsavel != null ?
                                                this.state.cidadesNaturalidadeResponsavel.map(function(cidade){
                                                    return (
                                                        <option key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                                                    );
                                                }, this)
                                                : <option value="" disabled>Cidade</option>
                                            }
                                        </select>
                                    </div>
                                </div>
                            </div>
                            : <div></div>
                        }

                        <h2 className="h3-responsive titulo mt-4">Endereço</h2>
                        <hr className="linha-hr" />

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-2">
                                <MDBInput label="CEP" id="cep" group type="text" validate error="wrong" success="right" value={this.state.endereco.cep} 
                                    onChange={e => this.setState( {endereco: {...this.state.endereco, cep: e.target.value}} )} onBlur={this.consultarCEP}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-5">
                                <MDBInput label="Rua" group type="text" validate error="wrong" success="right" value={this.state.endereco.logradouro} 
                                    onChange={e => this.setState( {endereco:{...this.state.endereco, logradouro: e.target.value}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-1">
                                <MDBInput label="Número" group type="text" validate error="wrong" success="right" value={this.state.endereco.numero} 
                                    onChange={e => {this.setState( {endereco:{...this.state.endereco, numero: e.target.value}} )}}/>
                            </div>

                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Bairro" group type="text" validate error="wrong" success="right" value={this.state.endereco.setor} 
                                    onChange={e => this.setState( {endereco:{...this.state.endereco, setor: e.target.value}} )}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Estado</span>
                                <br />
                                <select value={this.state.estado} onChange={e => this.setState({estado: e.target.value}, this.buscarCidades)} className="browser-default custom-select">
                                    <option value="" disabled>Estado</option>
                                    {this.state.estados.map(function(estado){
                                        return (
                                            <option key={estado.id} value={estado.id}>{estado.nome}</option>
                                        );
                                    }, this)}
                                </select>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Cidade</span>
                                <br />
                                <select value={this.state.endereco.cidadeId} onChange={e => this.setState( {endereco:{...this.state.endereco, cidadeId: e.target.value}} )} className="browser-default custom-select">
                                    <option value="" disabled>Cidade</option>
                                    { this.state.estado != null ?
                                        this.state.cidades.map(function(cidade){
                                            return (
                                                <option key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                                            );
                                        }, this)
                                        : <option value="" disabled>Cidade</option>
                                    }
                                </select>
                            </div>
                        </div>

                        <h2 className="h3-responsive titulo mt-4">Endereço Profissional</h2>
                        <hr className="linha-hr" />

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-2">
                                <MDBInput label="CEP" id="cep" group type="text" validate error="wrong" success="right" 
                                    value={this.state.enderecoProfissional.cep} 
                                    onChange={e => this.setState( {enderecoProfissional:{...this.state.enderecoProfissional, cep: cepMask(e.target.value)}} )} 
                                    onBlur={this.consultarCEPProfissional}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-5">
                                <MDBInput label="Rua" group type="text" validate error="wrong" success="right" value={this.state.enderecoProfissional.logradouro} 
                                    onChange={e => this.setState( {enderecoProfissional:{...this.state.enderecoProfissional, logradouro: e.target.value}} )}/>
                            </div>

                            <div className="md-form col-sm-12 col-xl-1">
                                <MDBInput label="Número" group type="text" validate error="wrong" success="right" value={this.state.enderecoProfissional.numero} 
                                    onChange={e => this.setState( {enderecoProfissional:{...this.state.enderecoProfissional, numero: e.target.value}} )}/>
                            </div>

                        </div>

                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-4">
                                <MDBInput label="Bairro" group type="text" validate error="wrong" success="right" value={this.state.enderecoProfissional.setor} 
                                    onChange={e => this.setState( {enderecoProfissional:{...this.state.enderecoProfissional, setor: e.target.value}} )}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Estado</span>
                                <br />
                                <select value={this.state.estadoProfissional} 
                                    onChange={e => this.setState({estadoProfissional: e.target.value}, this.buscarCidadesProfissional)} 
                                    className="browser-default custom-select">
                                    <option value="" disabled>Estado</option>
                                    {this.state.estadosProfissional.map(function(estado){
                                        return (
                                            <option key={estado.id} value={estado.id}>{estado.nome}</option>
                                        );
                                    }, this)}
                                </select>
                            </div>

                            <div className="md-form col-sm-12 col-xl-4">
                                <span>Cidade</span>
                                <br />
                                <select value={this.state.enderecoProfissional.cidadeId} 
                                    onChange={e => this.setState( {enderecoProfissional:{...this.state.enderecoProfissional, cidadeId: e.target.value}} )} 
                                    className="browser-default custom-select">
                                    <option value="" disabled>Cidade</option>
                                    { this.state.estado != null ?
                                        this.state.cidadesProfissional.map(function(cidade){
                                            return (
                                                <option key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                                            );
                                        }, this)
                                        : <option value="" disabled>Cidade</option>
                                    }
                                </select>
                            </div>
                        </div>
                    </form>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="light"
                        onClick={this.toggle('Paciente')}
                        className="btn btn-success my-4 waves-effect hoverable" >
                        Cancelar
                    </MDBBtn>
                    <MDBBtn color="success" 
                        className="btn btn-success my-4 waves-effect hoverable" 
                        onClick={() => this.salvarPaciente()}>
                        Salvar
                    </MDBBtn>
                </MDBModalFooter>
            </MDBModal>

            <SweetAlert
                show={this.state.swetAlert}
                title="Sucesso!"
                type="success"
                onEscapeKey={() => this.setState({ swetAlert: false })}
                onOutsideClick={() => this.setState({ swetAlert: false })}
                text={this.state.mensagem}
                onConfirm={() => this.setState({ swetAlert: false })}
            />
        
        </div>);
    }
}