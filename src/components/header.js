import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import logo from '../../assets/img/logo.jpg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faMap, faHandHoldingUsd, faStream, faCoins, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';

export default class Header extends Component{

    constructor(){
        super();
        this.state = {
            disabled: false,
            userAuth: ''
        }
    }

    componentWillMount(){
        // if(localStorage.getItem("userAuth") == null){
        //     this.setState({userAuth: 'invisible'})
        // }else{
        //     this.setState({userAuth: ''})
        // }
        // this.forceUpdate();
        this.setState({userAuth: ''})
    }

    render(){
        return(
            <div className={this.state.userAuth}>

                <nav id="menu-top" className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
                    <div className="container-fluid">

                        <Link className="navbar-brand waves-effect" to="/" >
                            <strong className="blue-text">DIGIDENTAL</strong>
                        </Link>

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">

                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link className="nav-link waves-effect" to="/aporte" >
                                        Home
                                        <span className="sr-only">(atual)</span>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link waves-effect" to="/usuario" >Usuário</Link>
                                </li>
                                {/* <li className="nav-item">
                                    <Link className="nav-link waves-effect" to="/aporte" >Aporte</Link>
                                </li> */}
                                <li className="nav-item">
                                    <Link className="nav-link waves-effect" to="/extrato" >Extrato</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link waves-effect" to="/movimentacao" >Movimentação</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link waves-effect" to="/financeiro" >Financeiro</Link>
                                </li>
                            </ul>

                        </div>

                    </div>
                </nav>

                <div className="sidebar-fixed position-fixed">

                    <Link className="logo-wrapper waves-effect" to="/">
                        <img src={logo} className="img-fluid" alt=""/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                        </button>

                    <div className="list-group list-group-flush">
                        <Link className="list-group-item active waves-effect" to="/" >
                            <FontAwesomeIcon icon={faMap} className="mr-3" />Home
                        </Link>
                        <Link className="list-group-item list-group-item-action waves-effect" to="/usuario" >
                            <FontAwesomeIcon icon={faUser} className="mr-3" /> Usuário
                        </Link>
                        <Link className="list-group-item list-group-item-action waves-effect" to="/aporte" >
                            <FontAwesomeIcon icon={faHandHoldingUsd} className="mr-3" />Aporte
                        </Link>
                        <Link className="list-group-item list-group-item-action waves-effect" to="/extrato" >
                            <FontAwesomeIcon icon={faStream} className="mr-3" />Extrato
                        </Link>
                        <Link className="list-group-item list-group-item-action waves-effect" to="/movimentacao" >
                            <FontAwesomeIcon icon={faCoins} className="mr-3" />Movimentação
                        </Link>
                        <Link className="list-group-item list-group-item-action waves-effect" to="/financeiro" >
                            <FontAwesomeIcon icon={faFileInvoiceDollar} className="mr-3" />Financeiro
                        </Link>

                    </div>

                </div>

            </div>
        );
    }
}