import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink } from 'mdbreact';
import decode from "jwt-decode";

class TopNavigation extends Component {
    state = {
        enumTipoUsuario: {
            1: 'Gestor',
            2: 'Agente',
            3: 'Cliente',
            4: 'Financeiro',
            5: 'TI',
            'Gestor': 1,
            'Agente': 2,
            'Cliente': 3,
            'Financeiro': 4,
            'TI': 5,
        },
        collapse: false,
        userAuth: 'flexible-navbar',
    }

    componentWillMount(){
        // if(localStorage.getItem("userAuth") == null){
        //     this.setState({userAuth: 'invisible'})
        // }else{
        //     this.setState({userAuth: 'flexible-navbar'})
        // }
        // this.forceUpdate();
        this.setState({userAuth: 'flexible-navbar'})
    }

    onClick = () => {
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    isCliente(){
        const decoded = localStorage.getItem('userAuth') != null ? decode(localStorage.getItem('userAuth')) : null
            
        if(decoded != null && parseInt(decoded.sub.tipoUsuario) === this.state.enumTipoUsuario['Cliente']){
            return true;
        }else{
            return false;
        }
        
    }

    render() {
        return (
            <MDBNavbar className={this.state.userAuth} light expand="md" scrolling>
                <MDBNavbarBrand href="/">
                    <strong>DIGIDENTAL</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick = { this.onClick } />
                <MDBCollapse isOpen = { this.state.collapse } navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem active onClick = { this.onClick }>
                            <MDBNavLink to="/aporte">Home</MDBNavLink>
                        </MDBNavItem>
                        {this.isCliente() ? 
                            null
                            : (<MDBNavItem onClick = { this.onClick }>
                                <MDBNavLink to="/usuario">Usuário</MDBNavLink>
                            </MDBNavItem>)
                        }
                        {this.isCliente() ? 
                            null
                            : (<MDBNavItem onClick = { this.onClick }>
                                <MDBNavLink to="/pessoa">Pessoas</MDBNavLink>
                            </MDBNavItem>)
                        }
                        {this.isCliente() ? 
                            null
                            : (<MDBNavItem onClick = { this.onClick }>
                                <MDBNavLink to="/gerenciar">Gerenciar</MDBNavLink>
                            </MDBNavItem>)
                        }
                        {/* <MDBNavItem onClick = { this.onClick }>
                            <MDBNavLink to="/aporte">Aporte</MDBNavLink>
                        </MDBNavItem> */}
                        <MDBNavItem onClick = { this.onClick }>
                            <MDBNavLink to="/perfil">Perfil</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar>
        );
    }
}

export default TopNavigation;