import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './breadcrumb.css';
import { MDBBtn, MDBIcon, MDBTooltip } from "mdbreact";

export default class Breadcrumb extends Component{
    constructor(props){
        super(props);
    }

    logout(){
        localStorage.removeItem('userAuth')
        window.location.pathname = '/login'
    }

    render(props){
        return(
            <div className="card mb-4 wow fadeIn">
                <div className="card-body d-sm-flex justify-content-between">
                    <h4 className="mb-2 mb-sm-0 pt-1">
                        <Link to="/aporte"><span className="colorBlue">Home</span></Link>
                        <span> / </span>
                        <span>{this.props.paginaAtual}</span>
                    </h4>

                    <span className="logout" onClick={this.logout.bind(this)}>
                        <MDBTooltip placement="left">
                            <MDBBtn  color="danger" className="btn-cancelar">
                                <MDBIcon icon="sign-out-alt" />
                            </MDBBtn>
                            <div>Logout</div>
                        </MDBTooltip>
                    </span>
                </div>
            </div>
        );
    }
}