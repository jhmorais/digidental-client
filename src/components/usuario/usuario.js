import React, { Component } from 'react';
import './usuario.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBInput, MDBDataTable, MDBIcon } from 'mdbreact';
import Breadcrumb from '../breadcrumb/breadcrumb';
import SweetAlert from 'sweetalert-react';
import helper from '../../helpers';
import {cpfMask, telefoneMask} from "../mask/mask";

export default class Usuario extends Component {

    constructor(props) {
        super(props);
        this.state = {
            enumTipoUsuario: {
                1: 'Gestor',
                2: 'Agente',
                3: 'Cliente',
                4: 'Financeiro',
                5: 'TI',
                'Gestor': 1,
                'Agente': 2,
                'Cliente': 3,
                'Financeiro': 4,
                'TI': 5,
            },
            usuarios:[],
            usuario:{},
            selectPessoa: "",
            selectConsultor: "",
            selectGestor: "",
            selectTipoUsuario: "",
            selectTipoUsuarioEditar: "",
            modal: false,
            modalEditarUsuario: false,
            modalExcluirUsuario: false,
            modalCadastroPessoa: false,
            pessoas:[],
            consultores:[],
            gestores:[],
            nome: '',
            login: '',
            taxaMajoracao: null,
            rendimentoVariavel: 0,
            porcentagemRendimento: "",
            email: '',
            cpf: '',
            telefone: '',
            usuarioExcluirId: 0,
            usuarioEditarId: 0,
            usuarioEditarTipoUsuario: 0,
            usuarioEditarConsultorId: 0,
            usuarioEditarGestorId: 0,
            usuarioEditarPessoaId: 0,
            loginEditar: '',
            taxaMajoracaoEditar: null,
            porcentagemRendimentoEditar: "",
            rendimentoVariavelEditar: 0,
            usuarioEditarPessoa: {
                nome: '',
                id: 0,
                email: '',
                telefone: 0,
                cpf: 0,
            },
            consultorEditar: {
                id: 0,
                usuarioId: 0,
                usuario:{
                    id:0,
                    pessoa:{
                        id: 0,
                        nome: ""
                    }
                }
            },
            usuarioEditarConsultor: {
                id: 0,
                usuarioId: 0,
                gestorId: 0
            },
            swetAlert: false,
            mensagem: ''
        };

        this.path = helper.path();
        this.toggle = this.toggle.bind(this);
        this.salvarPessoa = this.salvarPessoa.bind(this);
        this.salvarUsuario = this.salvarUsuario.bind(this);
    }

    componentDidMount(){
        this.atualizaPessoas();
        this.atualizaConsultores();
        this.atualizaGestores();
        this.atualizaUsuarios();
    }
    
    atualizaPessoas = () => {
        fetch(`${this.path}/pessoa/listaPessoasSemUsuario`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({pessoas:retorno.pessoas});
        });
    }

    atualizaConsultores = () => {
        fetch(`${this.path}/consultor/listaConsultores`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({consultores:retorno.consultores});
        });
    }

    atualizaGestores = () => {
        fetch(`${this.path}/gestor/listaGestores`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({gestores:retorno.gestores});
        });
    }

    atualizaUsuarios = () => {
        fetch(`${this.path}/usuario/listaUsuarios`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({usuarios:retorno.usuarios});
            this.montarRowsUsuarios();
        });
    }

    toggle = nomeModal => () => {
        let modal = 'modal' + nomeModal
        this.setState({
          [modal]: !this.state[modal]
        });
      }

    toggleExcluir = () => {
        this.setState({
            modalExcluirUsuario: !this.state.modalExcluirUsuario
        });
    }
    
    toggleEditar = () => {
        this.setState({
            modalEditarUsuario: !this.state.modalEditarUsuario
        });
    }

    setIdExcluir = (usuarioExcluirId) => {
        this.setState({usuarioExcluirId: usuarioExcluirId})
        this.toggleExcluir();
    }
    
    setEditar = (usuarioEditar) => {
        this.atualizaConsultores();
        this.atualizaGestores();
        fetch(`${this.path}/usuario/usuarioCompleto/` + usuarioEditar.id + '/' + usuarioEditar.tipoUsuario)
        .then(
            response => response.json()
        ).then(retorno => {
            console.log("usuario completo encontrado: ")
            console.log(retorno.usuario);
            debugger
            this.setState({
                usuarioEditarId: usuarioEditar.id,
                usuarioEditarPessoaId: usuarioEditar.pessoaId,
                usuarioEditarTipoUsuario: usuarioEditar.tipoUsuario,
                usuarioEditarPessoa: usuarioEditar.pessoa,
                usuarioEditarConsultorId: retorno.usuario.cliente.consultorId == null ? "" : retorno.usuario.cliente.consultorId,
                loginEditar: retorno.usuario.login,
                taxaMajoracaoEditar: retorno.usuario.taxaMajoracao,
                rendimentoVariavelEditar: retorno.usuario.cliente != undefined ? (retorno.usuario.cliente.rendimentoVariavel ? 1 : 0) : 0,
                porcentagemRendimentoEditar: retorno.usuario.cliente != undefined ? (retorno.usuario.cliente.porcentagemRendimento) : "",
            })
            if(retorno.usuario.tipoUsuario == this.state.enumTipoUsuario['Agente'])
                this.setState({usuarioEditarGestorId: retorno.usuario.consultor.gestorId})
            this.toggleEditar();
        });
    }
    
    salvarUsuario = (usuario) => {
        fetch(`${this.path}/usuario/createUsuario`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "pessoaId": this.state.selectPessoa,
                "login": this.state.login,
                "tipoUsuario": this.state.selectTipoUsuario,
                "consultorId": this.state.selectConsultor,
                "gestorId": this.state.selectGestor,
                "taxaMajoracao": this.state.taxaMajoracao,
                "rendimentoVariavel": this.state.rendimentoVariavel == 1 ? true : false,
                "porcentagemRendimento": this.state.porcentagemRendimento,
            })
        }).then(response => {
            return response.json();
        }).then(json => {
             this.atualizaUsuarios();
             this.setState({
                swetAlert: true, 
                selectPessoa: "",
                selectTipoUsuario: "",
                selectConsultor: "",
                selectGestor: "",
                login: "",
                taxaMajoracao: "",
                rendimentoVariavel: 0,
                porcentagemRendimento: "",
                mensagem: `Usuário salvo com sucesso.`
            })
        });

    }

    salvarPessoa = pessoa => {
        this.setState({modalCadastroPessoa: !this.state.modalCadastroPessoa});

        fetch(`${this.path}/pessoa/createPessoa`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "nome": this.state.nome,
                "email": this.state.email,
                "cpf": this.state.cpf.replace(/\D/g, ''),
                "telefone": this.state.telefone.replace(/\D/g, '')
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.atualizaPessoas();
            this.setState({
                nome: "",
                email: "",
                cpf: "",
                telefone: "",
                swetAlert: true, mensagem: `Pessoa salva com sucesso.`
            })
        });
    }

    excluirUsuario = () => {
        fetch(`${this.path}/usuario/deleteUsuario`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "usuarioId": this.state.usuarioExcluirId,
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.atualizaUsuarios();
            this.atualizaPessoas();
            this.toggleExcluir();
            this.setState({swetAlert: true, mensagem: `Usuário excluido com sucesso.`})
        });
    }
    
    editarUsuario = () => {
        fetch(`${this.path}/usuario/editUsuario`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "usuarioId": this.state.usuarioEditarId,
                "tipoUsuario": this.state.usuarioEditarTipoUsuario,
                "consultorId": this.state.usuarioEditarConsultorId,
                "gestorId": this.state.usuarioEditarGestorId,
                "login": this.state.loginEditar,
                "taxaMajoracao": this.state.taxaMajoracaoEditar,
                "rendimentoVariavel": this.state.rendimentoVariavelEditar == 1 ? true : false,
                "porcentagemRendimento": this.state.porcentagemRendimentoEditar,
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.atualizaUsuarios();
            this.toggleEditar();
            this.setState({swetAlert: true, taxaMajoracaoEditar: "", mensagem: `Usuário editado com sucesso.`})
        });
    }

    montarRowsUsuarios(){
        this.dataUsuarios = {
            columns: [
                {label: 'Pessoa', field: 'pessoa', sort: 'asc', width: 200},
                {label: 'Login',field: 'login', sort: 'asc', width: 200},
                {label: 'Tipo Usuário', field: 'tipoCliente', sort: 'asc', width: 100},
                {label: 'Editar', field: 'editar', width: 30},
                {label: 'Excluir', field: 'excluir', width: 30},
            ],
            rows: []
        }

        this.state.usuarios.map(usuario => {
            this.dataUsuarios.rows.push(
                {
                    pessoa: usuario.pessoa.nome,
                    login: usuario.login,
                    tipoCliente: this.state.enumTipoUsuario[usuario.tipoUsuario],
                    editar:  
                    <span className="btn btn-primary paddingButtonEdit" onClick={() => this.setEditar(usuario)}>
                        <FontAwesomeIcon icon={faEdit} value={usuario} className="displayBlock" />
                    </span>,
                    cancelar:  
                    <span className="btn btn-danger paddingButtonDelete" onClick={() => this.setIdExcluir(usuario.id)}>
                        <FontAwesomeIcon icon={faTrash} className="displayBlock" />
                    </span>
                }
            )
        })
        this.forceUpdate();
    }
    render() {
        return (
            <div id="content-father" className="container-fluid mt-5">

                <Breadcrumb paginaAtual="Usuário"/>

                <div className="row wow fadeIn">
                    <div className="col-12">
                        <div className="card">

                            <h5 className="card-header blue-gradient white-text text-center py-4">
                                <strong>Cadastro de Usuário</strong>
                            </h5>

                            <div className="card-body px-lg-5 pt-4">

                                <form className="text-left colorGrey">
                                    <div className="col-12">
                                        <span>Pessoa</span>
                                        <br />
                                        <select value={this.state.selectPessoa} onChange={e => this.setState({selectPessoa: e.target.value})} className="browser-default custom-select col-sm-12 col-xl-6">
                                            <option value="" disabled>Nome da Pessoa</option>

                                            {this.state.pessoas.map(function(pessoa){
                                                return (
                                                    <option key={pessoa.id} value={pessoa.id}>{pessoa.nome}</option>
                                                );
                                            }, this)}

                                        </select>
                                        <button type="button" className="btn btn-secondary btn-sm shadow" onClick={this.toggle('CadastroPessoa')}>
                                            Nova Pessoa
                                        </button>
                                    </div>
                                    <div className="col-sm-12 col-xl-3 mt-2">
                                        <span>Tipo</span>
                                        <br />
                                        <select value={this.state.selectTipoUsuario} onChange={e => this.setState({selectTipoUsuario: e.target.value})} className="browser-default custom-select">
                                            <option value="" disabled>Tipo do usuário</option>
                                            <option value="1">Gestor</option>
                                            <option value="2">Agente</option>
                                            <option value="3">Cliente</option>
                                            <option value="4">Financeiro</option>
                                            <option value="5">TI</option>
                                        </select>
                                    </div>
                                    
                                    {this.state.selectTipoUsuario == this.state.enumTipoUsuario['Cliente'] ?
                                        (
                                            <div className="row pl-3">
                                                <div className="col-6 mt-2">
                                                    <span>Consultor</span>
                                                    <br />
                                                    <select value={this.state.selectConsultor} onChange={e => this.setState({selectConsultor: e.target.value})} className="browser-default custom-select">
                                                        <option value="" disabled>Consultor</option>
                                                        {this.state.consultores.map(function(consultor){
                                                            return (
                                                                <option key={consultor.id} value={consultor.id}>{consultor.usuario.pessoa.nome}</option>
                                                            );
                                                        }, this)}
                                                    </select>
                                                </div>
                                            </div>
                                        )
                                        : (<div className="col-3 mt-2"></div>)
                                    }
                                    
                                    {this.state.selectTipoUsuario == this.state.enumTipoUsuario['Agente'] ?
                                        (
                                            <div className="col-3 mt-2">
                                                <span>Gestor</span>
                                                <br />
                                                <select value={this.state.selectGestor} onChange={e => this.setState({selectGestor: e.target.value})} className="browser-default custom-select">
                                                    <option value="" disabled>Gestor</option>
                                                    {this.state.gestores.map(function(gestor){
                                                        return (
                                                            <option key={gestor.id} value={gestor.id}>{gestor.usuario.pessoa.nome}</option>
                                                        );
                                                    }, this)}
                                                </select>
                                            </div>
                                        )
                                        : (<div className="col-3 mt-2"></div>)
                                    }
                                    
                                    <div className="col-6 mt-2">
                                        <span>Possui Rendimento Variável?</span>
                                        <br />
                                        <select value={this.state.rendimentoVariavel} onChange={e => this.setState({rendimentoVariavel: e.target.value})} className="browser-default custom-select">
                                            <option value="" disabled>Rendimento Variável</option>
                                            <option value="1">Sim</option>
                                            <option value="0">Não</option>
                                        </select>
                                    </div>

                                    {this.state.rendimentoVariavel == 1 ?
                                        (
                                            <div className="col-3 mt-3">
                                                <div className="md-form">
                                                    <MDBInput label="Porcentagem rendimento Variável" group type="text" validate error="wrong" success="right" 
                                                        value={this.state.porcentagemRendimento} onChange={e => this.setState({porcentagemRendimento: e.target.value})}/>
                                                </div>
                                            </div>
                                        )
                                        : (<div className="col-3 mt-2"></div>)
                                    }

                                    <div className="col-sm-12 col-xl-3 mt-4">
                                        <div className="md-form">
                                            <MDBInput label="Login" group type="text" validate error="wrong" success="right" 
                                                value={this.state.login} onChange={e => this.setState({login: e.target.value})}/>
                                        </div>
                                    </div>

                                    <div className="col-sm-12 col-xl-3 mt-4">
                                        <div className="md-form">
                                            <MDBInput label="Taxa de Majoração" group type="text" validate error="wrong" success="right" 
                                                value={this.state.taxaMajoracao} onChange={e => this.setState({taxaMajoracao: e.target.value})}/>
                                        </div>
                                    </div>
                                    
                                    <button className="btn btn-success btn-block my-4 waves-effect hoverable" type="button" onClick={this.salvarUsuario}>Salvar Usuário</button>
                                </form>

                                <h2 className="h3-responsive">Lista de Usuários</h2>
                                <hr />

                                <MDBDataTable striped bordered hover data={this.dataUsuarios} 
                                    entriesLabel="Mostrar "
                                    infoLabel={["Mostrando","até","de","entradas"]}
                                    paginationLabel={["Anterior","Próximo"]}
                                    searchLabel="Filtrar"
                                    responsive="true"/>

                                {/* <div className="table-responsive table-striped">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th className="th-lg width33">Pessoa</th>
                                                <th className="th-lg text-center width33">Login</th>
                                                <th className="th-lg text-center width33">Tipo</th>
                                                <th className="th-lg text-center width33">Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.usuarios.map(function(usuario){
                                                return (
                                                    <tr key={usuario.id}>
                                                        <td className="text-left">{usuario.pessoa.nome}</td>
                                                        <td>{usuario.login}</td>
                                                        <td className="text-center">{this.state.enumTipoUsuario[usuario.tipoUsuario]}</td>
                                                        <td className="text-center">
                                                            <span className="btn btn-primary paddingButtonEdit" onClick={() => this.setEditar(usuario)}>
                                                                <FontAwesomeIcon icon={faEdit} value={usuario} className="displayBlock" />
                                                            </span>
                                                            <span className="btn btn-danger paddingButtonDelete" value={usuario.id} onClick={() => this.setIdExcluir(usuario.id)}>
                                                                <FontAwesomeIcon icon={faTrash} className="displayBlock" />
                                                            </span>
                                                        </td>
                                                    </tr>
                                                );
                                            }, this)}
                                        </tbody>
                                    </table>

                                    <PaginationPage/>

                                </div> */}


                            </div>

                        </div>
                    </div>

                </div>

                <MDBModal isOpen={this.state.modalCadastroPessoa} toggle={this.toggle('CadastroPessoa')} fullHeight position="right">
                    <MDBModalHeader className="heading lead backgroundHeaderModal" toggle={this.toggle('CadastroPessoa')}>Cadastro Pessoa</MDBModalHeader>
                    <MDBModalBody>
                        <div className="text-center">
                            <i className="fas fa-user fa-4x mb-3 animated rotateIn icon"></i>
                        </div>
                        <form className="colorGrey">

                            <div className="md-form">
                                <MDBInput label="Nome" group type="text" validate error="wrong" success="right" value={this.state.nome} onChange={e => this.setState({nome: e.target.value})}/>
                            </div>

                            <div className="md-form">
                                <MDBInput label="E-mail" group type="email" validate error="wrong" success="right" value={this.state.email} onChange={e => this.setState({email: e.target.value})}/>
                            </div>

                            <div className="md-form">
                                <MDBInput label="CPF" group type="text" validate error="wrong" success="right" value={this.state.cpf} onChange={e => this.setState({cpf: cpfMask(e.target.value)})}/>
                            </div>

                            <div className="md-form">
                                <MDBInput label="Telefone" group type="text" validate error="wrong" success="right" value={this.state.telefone} onChange={e => this.setState({telefone: telefoneMask(e.target.value)})}/>
                            </div>

                        </form>
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn className="btn btn-light" onClick={this.toggle('CadastroPessoa')}>Cancelar</MDBBtn>
                        <MDBBtn color="success" onClick={this.salvarPessoa}>Salvar</MDBBtn>
                    </MDBModalFooter>
                </MDBModal>

                <MDBModal isOpen={this.state.modalEditarUsuario} toggle={this.toggleEditar}>
                    <MDBModalHeader toggle={this.toggleEditar}>Editar Usuário</MDBModalHeader>
                    <MDBModalBody>
                        <form className="text-left colorGrey">
                            <div className="col-12">
                                <span>Pessoa</span>
                                <br />
                                <select value={this.state.usuarioEditarId} disabled className="browser-default custom-select col-12">
                                    {<option key={this.state.usuarioEditarPessoa.id} value={this.state.usuarioEditarPessoa.id}>{this.state.usuarioEditarPessoa.nome}</option>}
                                </select>
                            </div>
                            <div className="col-12 mt-2">
                                <span>Tipo</span>
                                <br />
                                <select value={this.state.usuarioEditarTipoUsuario} onChange={e => this.setState({usuarioEditarTipoUsuario: e.target.value})} className="browser-default custom-select col-6">
                                    <option value="" disabled>Tipo do usuário</option>
                                    <option value="1">Gestor</option>
                                    <option value="2">Consultor</option>
                                    <option value="3">Cliente</option>
                                    <option value="4">Financeiro</option>
                                    <option value="5">TI</option>
                                </select>
                            </div>

                            {this.state.usuarioEditarTipoUsuario == this.state.enumTipoUsuario['Cliente'] ?
                                (
                                    <div className="row pl-3">
                                        <div className="col-10 mt-2">
                                            <span>Consultor</span>
                                            <br />
                                            <select value={this.state.usuarioEditarConsultorId} onChange={e => this.setState({usuarioEditarConsultorId: e.target.value})} className="browser-default custom-select col-12">
                                                <option value="0" disabled>Consultor</option>
                                                {this.state.consultores.map(function(consultor){
                                                    return (
                                                        <option key={consultor.id} value={consultor.id}>{consultor.usuario.pessoa.nome}</option>
                                                    );
                                                }, this)}
                                            </select>
                                        </div>
                                    </div>
                                )
                                : (<div className="col-12"></div>)
                            }

                            {this.state.usuarioEditarTipoUsuario == this.state.enumTipoUsuario['Agente'] ?
                                (
                                    <div className="col-12">
                                        <span>Gestor</span>
                                        <br />
                                        <select value={this.state.usuarioEditarGestorId} onChange={e => this.setState({usuarioEditarGestorId: e.target.value})} className="browser-default custom-select col-12">
                                            <option value="0" disabled>Gestor</option>
                                            {this.state.gestores.map(function(gestor){
                                                return (
                                                    <option key={gestor.id} value={gestor.id}>{gestor.usuario.pessoa.nome}</option>
                                                );
                                            }, this)}
                                        </select>
                                    </div>
                                )
                                : (<div className="col-12"></div>)
                            }

                            <div className="col-10 mt-2">
                                <span>Possui Rendimento Variável?</span>
                                <br />
                                <select value={this.state.rendimentoVariavelEditar} onChange={e => this.setState({rendimentoVariavelEditar: e.target.value})} className="browser-default custom-select">
                                    <option value="" disabled>Rendimento Variável</option>
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </div>
                            
                            {this.state.rendimentoVariavelEditar == 1 ?
                                (
                                    <div className="col-12 mt-3">
                                        <div className="md-form">
                                            <MDBInput label="Porcentagem rendimento Variável" group type="text" validate error="wrong" success="right" 
                                                value={this.state.porcentagemRendimentoEditar} onChange={e => this.setState({porcentagemRendimentoEditar: e.target.value})}/>
                                        </div>
                                    </div>
                                )
                                : (<div className="col-3 mt-2"></div>)
                            }

                            {this.state.usuarioEditarTipoUsuario == this.state.enumTipoUsuario['TI'] ?
                                (
                                    <div className="col-12 mt-3">
                                        <div className="md-form">
                                            <MDBInput label="Porcentagem rendimento Variável" group type="text" validate error="wrong" success="right" 
                                                value={this.state.porcentagemRendimentoEditar} onChange={e => this.setState({porcentagemRendimentoEditar: e.target.value})}/>
                                        </div>
                                    </div>
                                )
                                : (<div className="col-3 mt-2"></div>)
                            }

                            <div className="col-sm-12 col-xl-12 mt-4">
                                <div className="md-form">
                                    <MDBInput label="Login" group type="text" validate error="wrong" success="right" 
                                        value={this.state.loginEditar} onChange={e => this.setState({loginEditar: e.target.value})}/>
                                </div>
                            </div>

                            <div className="col-sm-12 col-xl-12 mt-4">
                                <div className="md-form">
                                    <MDBInput label="Taxa de Majoração" group type="text" validate error="wrong" success="right" 
                                        value={this.state.taxaMajoracaoEditar} onChange={e => this.setState({taxaMajoracaoEditar: e.target.value})}/>
                                </div>
                            </div>
                            
                        </form>
                    </MDBModalBody>
                    <MDBModalFooter>
                    <MDBBtn className="btn btn-light" onClick={this.toggleEditar}>Fechar</MDBBtn>
                    <MDBBtn color="primary" onClick={this.editarUsuario}>Editar Usuário</MDBBtn>
                    </MDBModalFooter>
                </MDBModal>

                <MDBModal isOpen={this.state.modalExcluirUsuario} toggle={this.toggleExcluir}>
                    <MDBModalHeader toggle={this.toggleExcluir}>Tem certeza que deseja excluir o Usuário?</MDBModalHeader>
                    <MDBModalFooter>
                        <MDBBtn className="btn btn-light" onClick={this.toggleExcluir}>Cancelar</MDBBtn>
                        <MDBBtn color="danger" onClick={this.excluirUsuario}>Excluir Usuário</MDBBtn>
                    </MDBModalFooter>
                </MDBModal>

            <SweetAlert
                show={this.state.swetAlert}
                title="Sucesso!"
                type="success"
                onEscapeKey={() => this.setState({ swetAlert: false })}
                onOutsideClick={() => this.setState({ swetAlert: false })}
                text={this.state.mensagem}
                onConfirm={() => this.setState({ swetAlert: false })}
            />

            </div>
        );
    }
}
