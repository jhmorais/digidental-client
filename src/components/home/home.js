import React, { Component } from 'react';
import './home.css';
import {Link} from 'react-router';
import Breadcrumb from '../breadcrumb/breadcrumb';

export default class Home extends Component {
    render() {
        return (
            <div id="content-father" className="container-fluid mt-5">

                <Breadcrumb paginaAtual=""/>

                <div className="row wow fadeIn">
                    <div className="col-12 col-xl-4 mb-4">
                        <div className="card">
                            <h5 className="card-header warning-color white-text text-center py-4">
                                <strong>Hash BTC</strong>
                            </h5>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-12 marginTop">
                                        <div className="md-form">
                                            <input type="text" id="hashBTC" className="form-control" />
                                            <label>Hash BTC</label>
                                            <button type="button" className="btn btn-amber col-12 marginLeft" >Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-12 col-xl-4 mb-4">
                        <div className="card">
                            <h5 className="card-header success-color white-text text-center py-4">
                                <strong>Taxa de Majoração</strong>
                            </h5>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-12 marginTop">
                                        <div className="md-form">
                                            <h2 className="text-center">9.97%</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div className="col-12 col-xl-4 mb-4">
                        <div className="card">
                            <h5 className="card-header default-color white-text text-center py-4">
                                <strong>Aportes</strong>
                            </h5>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-12 marginTop">
                                        <p className="height15px">
                                            <span className="floatLeft">Montante:</span>
                                            <span className="floatRight">R$ 5.2411,23</span>
                                        </p>
                                        <hr />
                                        <p className="height15px">
                                            <span className="floatLeft">Rendimento</span>
                                            <span className="floatRight">R$ 5.620,69</span>
                                        </p>
                                        <hr />
                                        <p className="height15px">
                                            <span className="floatLeft">Acumulado</span>
                                            <span className="floatRight">R$ 58.431,69</span>
                                        </p>
                                        <hr />
                                        <button type="button" className="btn btn-success col-12 marginLeft" >Detalhar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}

                    {/* <div className="col-12 col-xl-4 mb-4">
                        <div className="card">
                            <h5 className="card-header secondary-color white-text text-center py-4">
                                <strong>Movimentação</strong>
                            </h5>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-12 marginTop">
                                        <p className="height15px">
                                            <span className="floatLeft">Saldo:</span>
                                            <span className="floatRight">R$ 15.3260,00</span>
                                        </p>
                                        <hr />
                                        <p className="height15px">
                                            <span className="floatLeft">Bloqueado</span>
                                            <span className="floatRight">R$ 2.150,00</span>
                                        </p>
                                        <hr />
                                        <p className="height15px">
                                            <span className="floatLeft">Disponível</span>
                                            <span className="floatRight">R$ 58.431,00</span>
                                        </p>
                                        <hr />
                                        <button type="button" className="btn btn-purple col-12 marginLeft">Detalhar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}

                </div>
            </div>
        );
    }
}