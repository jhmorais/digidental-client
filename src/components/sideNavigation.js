import React, { Component } from 'react';
import { MDBListGroup, MDBListGroupItem, MDBIcon } from 'mdbreact';
import { NavLink, Link } from 'react-router-dom';
import {logo} from '../assets/img/logo.png';
import decode from "jwt-decode";

export default class SideNavigation extends Component {

    constructor(){
        super();
        this.state = {
            enumTipoUsuario: {
                1: 'Gestor',
                2: 'Agente',
                3: 'Cliente',
                4: 'Financeiro',
                5: 'TI',
                'Gestor': 1,
                'Agente': 2,
                'Cliente': 3,
                'Financeiro': 4,
                'TI': 5,
            },
            disabled: false,
            userAuth: 'sidebar-fixed position-fixed',
        }
    }

    isClienteOrAgente(){
        const decoded = localStorage.getItem('userAuth') != null ? decode(localStorage.getItem('userAuth')) : null
            
        if(decoded != null && (parseInt(decoded.sub.tipoUsuario) === this.state.enumTipoUsuario['Cliente'] || parseInt(decoded.sub.tipoUsuario) === this.state.enumTipoUsuario['Agente'])){
            return true;
        }else{
            return false;
        }
        
    }

    componentWillMount(){
        // if(localStorage.getItem("userAuth") === null){
        //     this.setState({userAuth: 'invisible'})
        // }else{
        //     this.setState({userAuth: 'sidebar-fixed position-fixed'})
        // }
        // this.forceUpdate();
        this.setState({userAuth: 'sidebar-fixed position-fixed'})
    }
    

    render(){
        return (
            <div className={this.state.userAuth} disabled={this.state.disabled}>
                
                <img src={require('../assets/img/logo.png')} className="img-fluid img-menu" alt=""/>

                <MDBListGroup className="list-group-flush">
                    {/* <NavLink exact={true} to="/aporte" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="map" className="mr-3"/>
                            Home
                        </MDBListGroupItem>
                    </NavLink> */}
                    <NavLink to="/paciente" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="user" className="mr-3"/>
                            Paciente
                        </MDBListGroupItem>
                    </NavLink>
                    {/* {this.isClienteOrAgente() ? 
                        null
                        : (
                            <NavLink to="/pessoa" activeClassName="activeClass">
                                <MDBListGroupItem>
                                    <MDBIcon icon="users" className="mr-3"/>
                                    Pessoas
                                </MDBListGroupItem>
                            </NavLink>
                        )
                    }
                    {this.isClienteOrAgente() ? 
                        null
                        : (
                            <NavLink to="/gerenciar" activeClassName="activeClass">
                                <MDBListGroupItem>
                                    <MDBIcon icon="briefcase" className="mr-3"/>
                                    Gerenciar
                                </MDBListGroupItem>
                            </NavLink>
                        )
                    } */}
                    {/* <NavLink to="/aporte" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="hand-holding-usd" className="mr-3"/>
                            Aporte
                        </MDBListGroupItem>
                    </NavLink> */}
                    {/* <NavLink to="/perfil" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="users-cog" className="mr-3"/>
                            Perfil
                        </MDBListGroupItem>
                    </NavLink> */}
                    {/* <NavLink to="/extrato" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="stream" className="mr-3"/>
                            Extrato
                        </MDBListGroupItem>
                    </NavLink>
                    <NavLink to="/movimentacao" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="coins" className="mr-3"/>
                            Movimentação
                        </MDBListGroupItem>
                    </NavLink>
                    <NavLink to="/financeiro" activeClassName="activeClass">
                        <MDBListGroupItem>
                            <MDBIcon icon="file-invoice-dollar" className="mr-3"/>
                            Financeiro
                        </MDBListGroupItem>
                    </NavLink> */}
                </MDBListGroup>
            </div>
        );
    }
}
