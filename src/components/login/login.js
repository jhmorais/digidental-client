import React, { Component } from 'react';
import './login.css';
import { MDBInput } from 'mdbreact';
import helper from '../../helpers';

export default class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            senha: '',
            login: ''
        }
        this.path = helper.path();
    }

    logar = () => {
        fetch(`${this.path}/api/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache': 'no-store',
                "Authorization": ''
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                "userName": this.state.login,
                "password": this.state.senha,
            })
        }).then(response => {
            if(response.ok){
                return response.json()
            }else{
                throw new Error('Error')
            }
        }).then(json => { 
            console.log(json) 
            if(json.access_token == undefined){
                alert(json.message)
            }else{
                localStorage.setItem('userAuth', json.access_token)
                window.location.pathname = '/aporte'
            }
        }).catch(error => {
            console.log(error.message)
        });
    }

    teste = () => {
        // this.props.history.push('/')
        localStorage.setItem('userAuth', {user: "jhmorais", password: "123456"})
        window.location.pathname = '/'
    }

    render(){
        return (
            <div className="row h-100 margin-top-50">
                <div className="col-sm-12 my-auto">
                    <div className="col-sm-6 col-xl-3 mx-auto">
                        <div className="card">

                            <h5 className="card-header blue-gradient white-text text-center py-4">
                                <strong>Bem Vindo</strong>
                            </h5>

                            <div className="card-body px-lg-5 pt-4">

                                <form className="color-grey">

                                    <div className="md-form">
                                        <MDBInput label="Usuário" group type="email" validate error="wrong" success="right" value={this.state.login} onChange={e => this.setState({login: e.target.value})}/>
                                    </div>

                                    <div className="md-form">
                                        <MDBInput label="Senha" group type="password" validate error="wrong" success="right" value={this.state.senha} onChange={e => this.setState({senha: e.target.value})}/>
                                    </div>

                                    <div className="d-flex justify-content-around">
                                        <div>
                                            <div className="form-check">
                                                <input type="checkbox" className="form-check-input" id="materialLoginFormRemember"/>
                                                <label className="form-check-label">Lembrar-me</label>
                                            </div>
                                        </div>
                                        {/* <div>
                                            <a href="">Esqueceu a senha?</a>
                                        </div> */}
                                    </div>

                                    <button id="acaoLogar" className="btn btn-primary btn-rounded-10 btn-block my-4 waves-effect z-depth-0" onClick={this.logar.bind(this)} type="button">
                                        Entrar
                                    </button>

                                    {/* <p>Não é um membro?
                                        <a href="" className="color-link"> Registrar</a>
                                    </p> */}


                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    };
}