import React, { Component } from 'react';
import { MDBFooter } from 'mdbreact';
import { Link } from "react-router-dom";

export default class Footer extends Component {

    constructor(){
        super();
        this.state = {
            disabled: false,
            userAuth: 'text-center font-small darken-2'
        }
    }

    componentWillMount(){
        // if(localStorage.getItem("userAuth") == null){
        //     this.setState({userAuth: 'invisible'})
        // }else{
        //     this.setState({userAuth: 'text-center font-small darken-2'})
        // }
        // this.forceUpdate();
        this.setState({userAuth: 'text-center font-small darken-2'})
    }

    render(){
        return (
            <MDBFooter color="blue" className={this.state.userAuth}>
                <div className="pt-4">
                    <hr className="my4"/>
                </div>
                <p className="footer-copyright mb-0 py-3 text-center">
                    &copy; {new Date().getFullYear()} Copyright: <Link to="/"> DIGIDENTAL </Link>
                </p>
            </MDBFooter>
        );
    }
}
