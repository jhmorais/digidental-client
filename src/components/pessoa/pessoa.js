import React, { Component } from 'react';
import './pessoa.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PaginationPage from '../pagination/pagination';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBInput, MDBDataTable } from 'mdbreact';
import Breadcrumb from '../breadcrumb/breadcrumb';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import helper from '../../helpers';
import {cpfMask, telefoneMask, cepMask} from "../mask/mask";

export default class Pessoa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            email: "",
            cpf: "",
            telefone: "",
            profissao: "",
            cidadeId: 0,
            endereco: "",
            cep: "",
            complemento: "",
            numero: "",
            bairro: "",
            estadoCivil: "",
            status: true,
            pessoaIdEditar: 0,
            pessoaIdExcluir: 0,
            modalEditarPessoa: false,
            modalExcluirPessoa: false,
            pessoas: [],
            swetAlert: false,
            mensagem: "",
            show: false
        }

        this.path = helper.path();

        this.enumEstadoCivil = {
            1: "Solteiro(a)",
            2: "Casado(a)",
            3: "Divorciado(a)",
            4: "Viúvo(a)",
            "Solteiro(a)": 1,
            "Casado(a)": 2,
            "Divorciado(a)": 3,
            "Viúvo(a)": 4
        }
        this.salvarPessoa = this.salvarPessoa.bind(this);
        this.atualizaPessoas = this.atualizaPessoas.bind(this);
        this.toggleEditar = this.toggleEditar.bind(this);
        this.toggleExcluir = this.toggleExcluir.bind(this);
        this.setEditar = this.setEditar.bind(this);
        this.setIdExcluir = this.setIdExcluir.bind(this);
        this.excluirPessoa = this.excluirPessoa.bind(this);
    }

    componentDidMount(){
        this.atualizaPessoas();
    }

    limparForm(json){
        this.setState({
            nome: "",
            email: "",
            cpf: "",
            telefone: "",
            mensagem: json.message,
            swetAlert: true,
        }, this.atualizaPessoas)
    }

    salvarPessoa() {
        fetch(`${this.path}/pessoa/createPessoa`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "nome": this.state.nome,
                "email": this.state.email,
                "cpf": this.state.cpf.replace(/\D/g, ''),
                "telefone": this.state.telefone.replace(/\D/g, '')
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.atualizaPessoas();
            this.limparForm(json);
        });
    }

    
    atualizaPessoas = () => {
        fetch(`${this.path}/pessoa/listaPessoas`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({pessoas:retorno.pessoas});
            this.montarRowsPessoas();
        });
    }

    setEditar(pessoaEditar) {
        console.log("pessoaEditar: ")
        console.log(pessoaEditar)
        fetch(`${this.path}/pessoa/` + pessoaEditar.id)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                nomeEditar: retorno.pessoa.nome,
                cpfEditar: retorno.pessoa.pessoaFisica != null ? retorno.pessoa.pessoaFisica.cpf.replace(/\D/g, '') : "",
                emailEditar: retorno.pessoa.email,
                telefoneEditar: retorno.pessoa.telefone.replace(/\D/g, ''),
                pessoaIdEditar: retorno.pessoa.id
            }, this.toggleEditar)
        });
    }

    toggleEditar = () => {
        this.setState({
            modalEditarPessoa: !this.state.modalEditarPessoa
        });
    }

    toggleExcluir = () => {
        this.setState({
            modalExcluirPessoa: !this.state.modalExcluirPessoa
        });
    }

    setIdExcluir(pessoaIdExcluir) {
        this.setState({pessoaIdExcluir: pessoaIdExcluir})
        this.toggleExcluir();
    }

    excluirPessoa(){
        fetch(`${this.path}/pessoa/deletePessoa`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "pessoaId": this.state.pessoaIdExcluir
            })
        }).then(
            response => response.json()
        ).then(json => {
            console.log(json) 
            this.atualizaPessoas();
            this.toggleExcluir();
            if(json.status == "Fail"){
                this.setState({mensagem: json.message, swetAlert: true})
            }
            
            console.log(this.state)
        });
    }

    editarPessoa = () => {
        fetch(`${this.path}/pessoa/editPessoa`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "pessoaId": this.state.pessoaIdEditar,
                "nome": this.state.nomeEditar,
                "email": this.state.emailEditar,
                "telefone": this.state.telefoneEditar.replace(/\D/g, ''),
                "cpf": this.state.cpfEditar.replace(/\D/g, '')
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.atualizaPessoas();
            this.toggleEditar();
        });
    }

    montarRowsPessoas(){
        this.dataPessoas = {
            columns: [
                {label: 'Nome', field: 'nome', sort: 'asc', width: 200},
                {label: 'E-mail',field: 'email', sort: 'asc', width: 200},
                {label: 'CPF', field: 'cpf', sort: 'asc', width: 100},
                {label: 'Telefone', field: 'telefone', width: 100},
                {label: 'Editar', field: 'editar', width: 30},
                {label: 'Excluir', field: 'excluir', width: 30},
            ],
            rows: []
        }

        this.state.pessoas.map(pessoa => {
            this.dataPessoas.rows.push(
                {
                    nome: pessoa.nome,
                    email: pessoa.email,
                    cpf: pessoa.pessoaFisica != null ? cpfMask(pessoa.pessoaFisica.cpf) : "-",
                    telefone: telefoneMask(pessoa.telefone),
                    editar:  
                    <span className="btn btn-primary paddingButtonEdit" onClick={() => this.setEditar(pessoa)}>
                        <FontAwesomeIcon icon={faEdit} value={pessoa} className="displayBlock" />
                    </span>,
                    excluir:  
                    <span className="btn btn-danger paddingButtonDelete" onClick={() => this.setIdExcluir(pessoa.id)}>
                        <FontAwesomeIcon icon={faTrash} className="displayBlock" />
                    </span>
                }
            )
        })
        this.forceUpdate();
    }

    render(){
        return(
            <div id="content-father" className="container-fluid mt-5">

            <Breadcrumb paginaAtual="Pessoa"/>

            <div className="row wow fadeIn">
                <div className="col-12">
                    <div className="card">

                        <h5 className="card-header blue-gradient white-text text-center py-4">
                            <strong>Cadastro de Pessoa</strong>
                        </h5>

                        <div className="card-body px-lg-5 pt-4">
                            <form className="text-left colorGrey">
                                <div className="row">
                                    <div className="md-form col-12">
                                        <MDBInput label="Nome" group type="text" validate error="wrong" success="right" value={this.state.nome} onChange={e => this.setState({nome: e.target.value})}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-6">
                                        <MDBInput label="E-mail" group type="text" validate error="wrong" success="right" value={this.state.email} onChange={e => this.setState({email: e.target.value})}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="CPF" group type="text" validate error="wrong" success="right" value={this.state.cpf} onChange={e => this.setState({cpf: cpfMask(e.target.value)})}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Telefone" group type="text" validate error="wrong" success="right" value={this.state.telefone} onChange={e => this.setState({telefone: telefoneMask(e.target.value)})}/>
                                    </div>
                                </div>
                                
                                
                                <button className="btn btn-success btn-block my-4 waves-effect hoverable" type="button" onClick={this.salvarPessoa}>Salvar Pessoa</button>
                            </form>
                            
                            <h2 className="h3-responsive">Lista de Pessoas</h2>
                            <hr />

                            <MDBDataTable striped bordered hover data={this.dataPessoas} 
                                entriesLabel="Mostrar "
                                infoLabel={["Mostrando","até","de","entradas"]}
                                paginationLabel={["Anterior","Próximo"]}
                                searchLabel="Filtrar"
                                responsive="true"/>
                        </div>
                    </div>
                </div>
            </div>

            <MDBModal isOpen={this.state.modalEditarPessoa} toggle={this.toggleEditar}>
                <MDBModalHeader toggle={this.toggleEditar}>Editar Pessoa</MDBModalHeader>
                <MDBModalBody>
                    <form className="text-left colorGrey">
                        
                    <div className="row">
                        <div className="md-form col-12">
                            <MDBInput label="Nome" group type="text" validate error="wrong" success="right" value={this.state.nomeEditar} onChange={e => this.setState({nomeEditar: e.target.value})}/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="md-form col-6">
                            <MDBInput label="E-mail" group type="text" validate error="wrong" success="right" value={this.state.emailEditar} onChange={e => this.setState({emailEditar: e.target.value})}/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="md-form col-4">
                            <MDBInput label="CPF" group type="text" validate error="wrong" success="right" value={this.state.cpfEditar} onChange={e => this.setState({cpfEditar: cpfMask(e.target.value)})}/>
                        </div>

                        <div className="md-form col-4">
                            <MDBInput label="Telefone" group type="text" validate error="wrong" success="right" value={this.state.telefoneEditar} onChange={e => this.setState({telefoneEditar: telefoneMask(e.target.value)})}/>
                        </div>
                    </div>
                        
                    </form>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn className="btn btn-light" onClick={this.toggleEditar}>Fechar</MDBBtn>
                    <MDBBtn color="primary" onClick={this.editarPessoa}>Editar Pessoa</MDBBtn>
                </MDBModalFooter>
            </MDBModal>

            
            <MDBModal isOpen={this.state.modalExcluirPessoa} toggle={this.toggleExcluir}>
                <MDBModalHeader toggle={this.toggleExcluir}>Tem certeza que deseja excluir a Pessoa?</MDBModalHeader>
                <MDBModalFooter>
                    <MDBBtn className="btn btn-light" onClick={this.toggleExcluir}>Cancelar</MDBBtn>
                    <MDBBtn color="danger" onClick={this.excluirPessoa}>Excluir Pessoa</MDBBtn>
                </MDBModalFooter>
            </MDBModal>

            <SweetAlert
                show={this.state.swetAlert}
                title="Sucesso!"
                type="success"
                onEscapeKey={() => this.setState({ swetAlert: false })}
                onOutsideClick={() => this.setState({ swetAlert: false })}
                text={this.state.mensagem}
                onConfirm={() => this.setState({ swetAlert: false })}
            />
        
        </div>);
    }
}