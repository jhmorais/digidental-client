import React, { Component } from 'react';
import './perfil.css';
import { MDBInput, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import Breadcrumb from '../breadcrumb/breadcrumb';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import helper from '../../helpers';
import decode from "jwt-decode";
import {cpfMask, telefoneMask, cepMask} from "../mask/mask";

export default class Perfil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pessoa: {cidadeId: "", profissao: ""},
            cpf: "",
            swetAlert: false,
            mensagem: "",
            usuarioId: 0,
            tipoUsuario: 0,
            estado: "",
            estados: [],
            cidades: [],
            modalAlterarSenha: false,
            senha: ""
        }

        this.path = helper.path();

        this.enumEstadoCivil = {
            1: "Solteiro(a)",
            2: "Casado(a)",
            3: "Divorciado(a)",
            4: "Viúvo(a)",
            "Solteiro(a)": 1,
            "Casado(a)": 2,
            "Divorciado(a)": 3,
            "Viúvo(a)": 4
        }
        this.salvarPessoa = this.salvarPessoa.bind(this);
    }

    componentDidMount(){
        this.getUsuarioLogado()
        this.buscarEstados()
    }

    getUsuarioLogado = () => {
        const decoded = localStorage.getItem('userAuth') != null ? decode(localStorage.getItem('userAuth')) : null
            
        if(decoded != null){
            this.setState({usuarioId: decoded.sub.userId, tipoUsuario: decoded.sub.tipoUsuario}, this.buscarPessoa)
        }
    }

    buscarPessoa = () => {
        fetch(`${this.path}/pessoa/pessoaPorUsuarioId/` + this.state.usuarioId)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                pessoa: retorno.pessoa, 
                cpf: cpfMask(retorno.pessoa.pessoaFisica.cpf), 
                estado: retorno.pessoa.cidade == null ? "" : retorno.pessoa.cidade.uf.id
            }, this.buscarCidades);
            this.setState({
                pessoa:{
                    ...this.state.pessoa, 
                    telefone: telefoneMask(this.state.pessoa.telefone), 
                    cep: cepMask(this.state.pessoa.cep)
                }
            })
        });
    }

    buscarCidades = () => {
        fetch(`${this.path}/cidade/cidadePorEstado/` + this.state.estado)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({cidades: retorno.cidades != undefined ? retorno.cidades : []});
        });
    }

    buscarEstados = () => {
        fetch(`${this.path}/uf/`)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({estados:retorno.estados});
        });
    }

    limparForm(json){
        this.setState({
            cpf: "",
            mensagem: json.message,
            swetAlert: true,
            estado: 0,
            cidades: [],
            senha: ""
        }, this.atualizaPessoas)
    }

    salvarPessoa = () => {
        fetch(`${this.path}/pessoa/editarPessoaPerfil`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "pessoa": this.state.pessoa,
                "cpf": this.state.cpf.replace(/\D/g, ''),
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.setState({
                swetAlert: true, 
                mensagem: `Pessoa atualizada com sucesso.`
            })
        });
    }

    toggle = nomeModal => () => {
        let modal = 'modal' + nomeModal
        this.setState({
          [modal]: !this.state[modal]
        });
    }
    
    alterarSenha = senha => {
        fetch(`${this.path}/usuario/alterarSenha`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": localStorage.getItem("userAuth")
            },
            body: JSON.stringify({
                "senha": this.state.senha,
                "usuarioId": this.state.usuarioId
            })
        }).then(response => {
            return response.json();
        }).then(json => { 
            this.limparForm(json);
            this.setState({mensagem: `Sua Senha foi alterada com sucesso!`, swetAlert: true, modalAlterarSenha: false})
        });
    }

    limparSenha = () => {
        this.setState({senha : ""})
    }

    consultarCEP = () => {
        var urlRestCEP = `https://viacep.com.br/ws/${this.state.pessoa.cep.replace(/\D/g, "")}/json/`
        fetch(urlRestCEP)
        .then(
            response => response.json()
        ).then(retorno => {
            this.setState({
                pessoa:{
                    ...this.state.pessoa,
                    cep: retorno.cep,
                    endereco: retorno.logradouro,
                    bairro: retorno.bairro,
                    complemento: retorno.complemento,
                    cidade: retorno.localidade,
                    estado: retorno.uf
                }
            })
        });
    }

    render(){
        return(
            <div id="content-father" className="container-fluid mt-5">

            <Breadcrumb paginaAtual="Perfil"/>

            <div className="row wow fadeIn">
                <div className="col-12">
                    <div className="card">

                        <h5 className="card-header blue-gradient white-text text-center py-4">
                            <strong>Perfil Usuário</strong>
                        </h5>

                        <div className="card-body px-lg-5 pt-4">
                            <form className="text-left colorGrey">

                                <h2 className="h3-responsive titulo">Dados Pessoais</h2>
                                <hr className="linha-hr" />

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-5"> 
                                        <MDBInput label="Nome" group type="text" validate error="wrong" success="right" value={this.state.pessoa.nome} onChange={e => this.setState( {pessoa: {...this.state.pessoa, nome: e.target.value}} ) }/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3">
                                        <MDBInput label="CPF" id="cpf" group type="text" validate error="wrong" success="right" value={this.state.cpf} onChange={e => this.setState({cpf: cpfMask(e.target.value)})}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="E-mail" group type="text" validate error="wrong" success="right" value={this.state.pessoa.email} onChange={e => this.setState( {pessoa: {...this.state.pessoa, email: e.target.value}} )}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Telefone" id="telefone" group type="text" validate error="wrong" success="right" value={this.state.pessoa.telefone} onChange={e => this.setState( {pessoa: {...this.state.pessoa, telefone: telefoneMask(e.target.value)}} )}/>
                                    </div>
                                
                                    <div className="md-form col-sm-12 col-xl-6">
                                        <MDBInput label="Profissão" group type="text" validate error="wrong" success="right" value={this.state.pessoa.profissao} onChange={e => this.setState( {pessoa: {...this.state.pessoa, profissao: e.target.value}} )}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-3 margin-bot">
                                        <span>Estado Civil</span>
                                        <br />
                                        <select value={this.state.pessoa.estadoCivil} onChange={e => this.setState( {pessoa: {...this.state.pessoa, estadoCivil: e.target.value}} )} className="browser-default custom-select">
                                            <option value="" disabled>:: Selecione ::</option>
                                            <option value="1">Solteiro(a)</option>
                                            <option value="2">Casado(a)</option>
                                            <option value="3">Divorciado(a)</option>
                                            <option value="4">Viúvo(a)</option>
                                        </select>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-12 margin-bot">
                                        <button type="button" className="btn btn-danger btn-sm" onClick={this.toggle('AlterarSenha')}>Alterar Senha</button>
                                    </div>

                                </div>
                                
                                <h2 className="h3-responsive titulo">Endereço</h2>
                                <hr className="linha-hr" />

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-2">
                                        <MDBInput label="CEP" id="cep" group type="text" validate error="wrong" success="right" value={this.state.pessoa.cep} onChange={e => this.setState( {pessoa: {...this.state.pessoa, cep: cepMask(e.target.value)}} )} onBlur={this.consultarCEP}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-5">
                                        <MDBInput label="Rua" group type="text" validate error="wrong" success="right" value={this.state.pessoa.endereco} onChange={e => this.setState( {pessoa: {...this.state.pessoa, endereco: e.target.value}} )}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-1">
                                        <MDBInput label="Número" group type="text" validate error="wrong" success="right" value={this.state.pessoa.numero} onChange={e => this.setState( {pessoa: {...this.state.pessoa, numero: e.target.value}} )}/>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-6">
                                        <MDBInput label="Complemento" group type="text" validate error="wrong" success="right" value={this.state.pessoa.complemento} onChange={e => this.setState( {pessoa: {...this.state.pessoa, complemento: e.target.value}} )}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <MDBInput label="Bairro" group type="text" validate error="wrong" success="right" value={this.state.pessoa.bairro} onChange={e => this.setState( {pessoa: {...this.state.pessoa, bairro: e.target.value}} )}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="md-form col-sm-12 col-xl-4">
                                        <span>Estado</span>
                                        <br />
                                        <select value={this.state.estado} onChange={e => this.setState({estado: e.target.value}, this.buscarCidades)} className="browser-default custom-select">
                                            <option value="" disabled>Estado</option>
                                            {this.state.estados.map(function(estado){
                                                return (
                                                    <option key={estado.id} value={estado.id}>{estado.nome}</option>
                                                );
                                            }, this)}
                                        </select>
                                    </div>

                                    <div className="md-form col-sm-12 col-xl-4">
                                        <span>Cidade</span>
                                        <br />
                                        <select value={this.state.pessoa.cidadeId} onChange={e => this.setState( {pessoa: {...this.state.pessoa, cidadeId: e.target.value}} )} className="browser-default custom-select">
                                            <option value="" disabled>Cidade</option>
                                            { this.state.estado != null ?
                                                this.state.cidades.map(function(cidade){
                                                    return (
                                                        <option key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                                                    );
                                                }, this)
                                                : <option value="" disabled>Cidade</option>
                                            }
                                        </select>
                                    </div>
                                </div>

                                <button className="btn btn-success btn-block my-4 waves-effect hoverable" type="button" onClick={this.salvarPessoa}>Salvar</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <MDBModal isOpen={this.state.modalAlterarSenha} toggle={this.toggle('AlterarSenha')} fullHeight position="right">
                <MDBModalHeader toggle={this.toggle('AlterarSenha')}>Alterar Senha</MDBModalHeader>
                <MDBModalBody>
                    <form className="text-left colorGrey">
                        
                    <div className="row">
                        <div className="md-form col-12">
                            <MDBInput label="Senha" group type="password" validate error="wrong" success="right" 
                                value={this.state.senha} onChange={e => this.setState({senha: e.target.value})}/>
                        </div>
                    </div>
                        
                    </form>
                    <div className="text-justify">
                        <p>
                            ** Após alterar, sua senha será atualizada.
                        </p>
                        <MDBBtn color="warning" size="sm" onClick={this.limparSenha}>Limpar Senha</MDBBtn>
                    </div>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn className="btn btn-light" onClick={this.toggle('AlterarSenha')}>Fechar</MDBBtn>
                    <MDBBtn color="success" onClick={this.alterarSenha}>Alterar</MDBBtn>
                </MDBModalFooter>
            </MDBModal>

            <SweetAlert
                show={this.state.swetAlert}
                title="Sucesso!"
                type="success"
                onEscapeKey={() => this.setState({ swetAlert: false })}
                onOutsideClick={() => this.setState({ swetAlert: false })}
                text={this.state.mensagem}
                onConfirm={() => this.setState({ swetAlert: false })}
            />
        
        </div>);
    }
}