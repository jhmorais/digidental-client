import React, {Component} from 'react';
import './App.css';
import './index.css';
import SideNavigation from './components/sideNavigation';
import TopNavigation from './components/topNavigation';
import Footer from './components/footer';
import Routes from './Routes.js'

export default class App extends Component {

  constructor(){
    super();
    this.state = {
        userAuth: 'p-5'
    }
  }

  componentWillMount(){
      // if(localStorage.getItem("userAuth") == null){
      //     console.log('nao logado')
      //     this.setState({userAuth: 'left-0'})
      // }else{
      //     console.log('logado')
      //     this.setState({userAuth: 'p-2'})
      //   }
      this.setState({userAuth: 'p-2'})
  }

  render(){
    return (
      <div className="flexible-content">
        <TopNavigation />
        <SideNavigation />
        <main id="content" className={this.state.userAuth}>
          <Routes />
        </main>
        <Footer />
      </div>
    );
  }
}