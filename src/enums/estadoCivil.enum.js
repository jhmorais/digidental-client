const estadoCivil = {
    "S": "Solteiro(a)",
    "C": "Casado(a)",
    "D": "Divorciado(a)",
    "V": "Viúvo(a)",
}

export default estadoCivil;