import React, {Component} from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import Home from './components/home/home';
import Usuario from './components/usuario/usuario';
import Login from './components/login/login';
import Pessoa from './components/pessoa/pessoa';
import decode from "jwt-decode";
import Perfil from './components/perfil/perfil';
import Paciente from './components/paciente/paciente';


export default class Routes extends Component{

    isTokenExpired(decoded){
        if (decoded.exp < Date.now() / 1000) {
            return true;
        } else return false;
    }

    logout(){
        localStorage.removeItem('userAuth')
        window.location.pathname = '/login'
    }
    
    verificaAutenticacao(){
        if(localStorage.getItem('userAuth') == null){
            window.location.pathname = '/login'
        }else{
            const decoded = decode(localStorage.getItem('userAuth'))
            if(this.isTokenExpired(decoded)){
                this.logout()
            }else{
                return true;
            }

        }
    }

    render(){
        return(
            <div>
            <Switch>
                <Route exact path="/" render={() => (<Paciente />)} />
                <Route exact path="/usuario" render={() => (<Usuario />)} />
                <Route exact path="/Pessoa" render={() => (<Pessoa />)} />
                <Route exact path="/Paciente" render={() => (<Paciente />)} />
                <Route exact path="/Perfil" render={() => (<Perfil />)} />
                <Route exact path="/Login" component={Login} />
            </Switch>
            {/* <Switch>
                <Route exact path="/" render={() => (this.verificaAutenticacao() ?(<Home />):(<Redirect to="/login" />))} />
                <Route exact path="/usuario" render={() => (this.verificaAutenticacao() ?(<Usuario />):(<Redirect to="/login" />))} />
                <Route exact path="/Pessoa" render={() => (this.verificaAutenticacao() ?(<Pessoa />):(<Redirect to="/login" />))} />
                <Route exact path="/Perfil" render={() => (this.verificaAutenticacao() ?(<Perfil />):(<Redirect to="/login" />))} />
                <Route exact path="/Login" component={Login} />
            </Switch> */}
            </div>
        );
    }
}